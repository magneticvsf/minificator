export class Urls {
    id: number;
    url: string;
    min_url: string;
    clicks: number;
    created_date: Date;
    fallow_date: Date;
}