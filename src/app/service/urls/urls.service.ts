import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Urls } from '../../models/urls'

@Injectable({
  providedIn: 'root'
})
export class UrlsService {

  hostname = 'http://localhost:12345';

  constructor(private http: HttpClient) { }

  getAll(page:any = null, limit:any = 10) {
    let pagination = ``;
    if(limit && page){
      pagination = `?limit=${limit}&page=${page}`;
    }
    return this.http.get<Urls[]>(`${this.hostname}/urls${pagination}`);
  }

  getClicks(link:string) {
    return this.http.get<Urls[]>(`${this.hostname}/url/clicks/${link}`);
  }
  getClicksCount() {
    return this.http.get<Urls[]>(`${this.hostname}/clicks/count`);
  }

  add(body:any) {
    return this.http.post(`${this.hostname}/url`, body);
  }
  
}
