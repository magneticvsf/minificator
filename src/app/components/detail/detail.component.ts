import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UrlsService } from 'src/app/service/urls/urls.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  
  hostname: string = 'http://localhost:12345';
  link: string = '';
  referers: Array<any> = [];
  clicks: Array<any>;
  labels: Array<string>;
  data: Array<number>;
  labelsBrowser: Array<string>;
  dataBrowser: Array<number>;
  labelsPlatform: Array<string>;
  dataPlatform: Array<number>;
  columnTypes: any;
  chartType: string;
  options: any;
  optionsPie: any;
  refererCount: number = 0;
  
  constructor(private route: ActivatedRoute,
              private urlsService: UrlsService) {
    this.route.params.subscribe(async (params) => {
      this.link = params.link;
      this.urlsService.getClicks(this.link).subscribe((clicks: any) => {
        this.clicks = clicks;
        this.SetValues();
      });
    });
  }

  ngOnInit() {
    this.columnTypes = [{
      'type': 'string',
      'value': 'Count clicks this link'
    },
    {
      'type': 'number',
      'value': 'Clicks'
    }];
    this.options = {
      'width': 1140,
      'height': 400,
      'bars': 'vertical',
      'chartArea': { 'left': 150, 'bottom': 50, 'right': 150, 'top': 50 }
    }
    this.optionsPie = {
      'width': 570,
      'height': 400,
      'bars': 'vertical',
      'chartArea': { 'left': 50, 'bottom': 20, 'right': 50, 'top': 20 }
    }
    this.SetValues();
  }

  SetValues() {
    this.data = [];
    this.labels = [];
    this.dataBrowser = [];
    this.labelsBrowser = [];
    this.dataPlatform = [];
    this.labelsPlatform = [];
    let dates = {};
    let referer = {};
    let browser = {};
    let os = {};
    let platform = {};
    this.clicks.map(click => {
      this.refererCount++;
      let date = click.created_date.substr(0, 10);
      if(dates[date]){
        dates[date] += 1;
      } else {
        dates[date] = 1;
      }
      if(referer[click.referer_url]){
        referer[click.referer_url] += 1;
      } else {
        referer[click.referer_url] = 1;
      }
      if(browser[click.browser]){
        browser[click.browser] += 1;
      } else {
        browser[click.browser] = 1;
      }
      if(os[click.os]){
        os[click.os] += 1;
      } else {
        os[click.os] = 1;
      }
      if(platform[click.platform]){
        platform[click.platform] += 1;
      } else {
        platform[click.platform] = 1;
      }
    });
    $.each(dates, (date, count) => {
      this.labels.push(date);
      this.data.push(count);
    });
    $.each(browser, (bw, count) => {
      this.labelsBrowser.push(bw);
      this.dataBrowser.push(count);
    });
    $.each(platform, (pf, count) => {
      this.labelsPlatform.push(pf);
      this.dataPlatform.push(count);
    });
    $.each(referer, (url, count) => {
      if(url != 'undefined') this.referers.push({url: url, count: count});
    });
  }

}
