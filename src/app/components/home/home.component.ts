import { Component, OnInit } from '@angular/core';
import { UrlsService } from '../../service/urls/urls.service';
import { map } from 'rxjs/operators';
import { Urls } from '../../models/urls';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit{

  page:number = 1;
  clicksCount:number = 0;
  urls: Urls;
  count: number = 0;
  hostname: string = 'http://localhost:12345';
  urlForm: FormGroup = new FormGroup({
    url: new FormControl('', [Validators.required, Validators.pattern("^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$")]),
    fallow: new FormControl('all', Validators.required)
  });

  constructor(public urlsService: UrlsService) { }

  ngOnInit() {
    this.getUrls();
    this.getClicksCount();
  }

  changePage(page){
    this.page = page;
    this.getUrls();
  }
  
  getUrls() {
    this.urlsService.getAll(this.page).subscribe((res: any) => {
      this.count = res.count;
      this.urls = res.items;
      console.log(res);
    });
  }

  getClicksCount() {
    this.urlsService.getClicksCount().subscribe((res: any) => {
      this.clicksCount = res.count;
      console.log(res);
    });
  }

  onSubmit() {
    if(this.urlForm.valid){
      let res:any = {};
      let fallow_date: Date;
      let date = new Date();
      let year = date.getFullYear();
      let seconds = date.getTime();
      switch(this.urlForm.value.fallow){
        case 'all' : fallow_date = new Date(seconds + (10 * 265 * 24 * 60 * 60 * 1000)); break;
        case 'week' : fallow_date = new Date(seconds + (7 * 24 * 60 * 60 * 1000)); break;
        case 'month' : fallow_date = new Date(seconds + (30 * 24 * 60 * 60 * 1000)); break;
      }
      res.fallow_date = fallow_date;
      res.url = this.urlForm.value.url;
      console.log(res);
      this.urlsService.add(res).subscribe((result:any) => {
        if(result){
          this.urlForm.value.url = '';
          this.urlForm.value.fallow = 'all';
          this.getUrls();
          this.getClicksCount();
        }
      });
    }
  }
}
